#!/usr/bin/env node
'use strict';

const commander = require('commander');
const packageInfo = require('../package.json');
const { release } = require('./index');

commander
  .version(packageInfo.version)
  .option('-t, --token [token]', 'GitLab access token')
  .option('-n, --npm', 'Publish to a public or private NPM registry')
  .option('--npmToken [npmToken]', 'npm registry authentication token')
  .option('--npmRegistry [npmRegistry]', 'npm registry url (defaults to the public npmjs.org registry)')
  .option('--releaseType [releaseType]', 'Semantic version increment to use. e.g. major, minor, patch')
  .option('--releaseVersion [releaseVersion]', 'Semantic version to use. e.g. 1.0.1, 1.7.5, 2.0.0')
  .parse(process.argv);

const npmToken = commander.npmToken;
const npmRegistry = commander.npmRegistry;
const settings = {
  npmToken,
  npmRegistry,
  withNpm: commander.npm || !!npmToken,
  token: commander.token,
  releaseType: commander.releaseType,
  releaseVersion: commander.releaseVersion,
};
console.log(`Creating a release ${settings.withNpm ? 'with' : 'without'} npm registry publishing`);

release(settings)
  .catch((err) => {
    if (err && err.type === 'NO_CHANGES') {
      console.log('No changes have been found since last release -> no release will be created!');
      process.exit();
    } else {
      console.error('Release failed', err);
      process.exit(1);
    }
  })
  .then((version) => {
    console.log(`Release successful: ${version}`);
    process.exit();
  });
