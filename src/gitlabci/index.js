'use strict';

const promisify = require('es6-promisify');
const Gitlab = require('gitlab');

const {
  CI_PROJECT_URL,
  CI_PROJECT_PATH,
} = process.env;

function addRelease({ token, version, hash, notes }) {
  const tag = `v${version}`;
  const host = /(https?:\/\/[^/]*)/.exec(CI_PROJECT_URL)[0];
  const client = new Gitlab({
    token,
    url: host,
  });

  const addTag = promisify(client.projects.repository.addTag);
  return addTag({
    id: CI_PROJECT_PATH, // e.g. 'ikhemissi/test-release-project'
    ref: hash,
    tag_name: tag,
    message: tag,
    release_description: notes,
  })
  .catch((err) => {
    // TODO: find out why the library is throwing an error even if there is no error
    if (err && err.release) { // Not an error.
      return err;
    }

    throw err;
  });
}

module.exports = {
  addRelease,
};
