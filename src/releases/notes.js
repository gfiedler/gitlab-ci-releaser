'use strict';

const handlebars = require('handlebars');
const template = require('./template');

const createReleaseChangelog = handlebars.compile(template);
const breakingChangeRegex = /breaking change/i;
const changeTypes = [
  { type: 'breakingChange', label: 'Breaking Changes' },
  { type: 'feat', label: 'Features' },
  { type: 'fix', label: 'Bug fixes' },
  { type: 'refactor', label: 'Code refactoring' },
  { type: 'perf', label: 'Performance Improvements' },
  { type: 'chore', label: 'Build process and tools' },
  { type: 'style', label: 'Code styling' },
  { type: 'docs', label: 'Documentation' },
  { type: 'test', label: 'Tests' },
  { type: 'other', label: 'Other changes' },
];

function formatCommit(commit) {
  const issues = commit.references.map(reference => reference.raw);
  const hasBreakingChanges = commit.notes.some(note => breakingChangeRegex.test(note.title));
  return {
    issues,
    type: hasBreakingChanges ? 'breakingChange' : commit.type,
    hash: commit.hash,
    subject: commit.subject,
  };
}

function getTypeDetails(commitType = 'other') {
  return changeTypes.find(details => details.type === commitType);
}

function groupByType(commits) {
  const groups = {};
  const otherChangesType = getTypeDetails('other');
  commits.forEach((commit) => {
    const commitType = getTypeDetails(commit.type) || otherChangesType;
    groups[commitType.type] = groups[commitType.type] || Object.assign({ list: [] }, commitType);
    groups[commitType.type].list.push(commit);
  });

  const sorted = changeTypes.map(changeType => groups[changeType.type]);
  return sorted.filter(group => !!group);
}

function generate(commits, version) {
  const details = commits.filter(commit => !!commit.type).map(commit => formatCommit(commit));
  const grouped = groupByType(details);
  return createReleaseChangelog({
    version,
    changes: grouped,
  });
}

module.exports = {
  generate,
};
