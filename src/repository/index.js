'use strict';

const taggedVersions = require('tagged-versions');
const commits = require('./commits');
const commitAndPush = require('./commitAndPush');

module.exports = {
  commitAndPush,
  getCommitList: commits.getCommitList,
  getLastVersion: taggedVersions.getLastVersion,
};
