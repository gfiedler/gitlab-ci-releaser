/* eslint-disable no-param-reassign */
'use strict';

const test = require('ava');
const sinon = require('sinon');
const path = require('path');

const dummyPackageJson = require('../helpers/mockPackageJson');
const packageJson = require('../../src/packageJson');

test('must return package.json contents', (t) => {
  return packageJson.getContent()
    .then((data) => {
      t.deepEqual(data, dummyPackageJson);
    });
});

test('must return package.json path', (t) => {
  t.is(packageJson.getPath(), path.join(process.cwd(), 'package.json'));
});
