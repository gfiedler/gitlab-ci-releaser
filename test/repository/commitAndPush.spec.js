/* eslint-disable no-param-reassign */
'use strict';

const test = require('ava');
const sinon = require('sinon');
const commands = require('../../src/utils/commands');

function mockEnvVariables(context, name, value) {
  context[name] = process.env[name];
  process.env[name] = value;
}

const commandCallMocks = {
  'git -c user.email="master@example.com" -c user.name="developer" commit -m commitmessage -- package.json': '',
  'git log -n 1 --pretty="%H"': 'commithash',
  'git remote add gitlabcireleaser-15 https://developer:authenticationtoken@gitlab.com/developer/test-release-project.git': '',
  'git push gitlabcireleaser-15 HEAD:master': '',
  'git remote rm gitlabcireleaser-15': '',
};

test.beforeEach((t) => {
  mockEnvVariables(t.context, 'CI_REPOSITORY_URL', 'https://gitlab-ci-token:xxxx@gitlab.com/developer/test-release-project.git');
  mockEnvVariables(t.context, 'CI_JOB_ID', '15');
  mockEnvVariables(t.context, 'CI_COMMIT_REF_NAME', 'master');
  mockEnvVariables(t.context, 'GITLAB_CI_RELEASER_NAME', 'developer');
  mockEnvVariables(t.context, 'GITLAB_CI_RELEASER_EMAIL', 'master@example.com');
  sinon.stub(commands, 'run', command => Promise.resolve(commandCallMocks[command]));
});

test.afterEach((t) => {
  process.env.CI_REPOSITORY_URL = t.context.CI_REPOSITORY_URL;
  process.env.CI_JOB_ID = t.context.CI_JOB_ID;
  process.env.CI_COMMIT_REF_NAME = t.context.CI_COMMIT_REF_NAME;
  process.env.GITLAB_CI_RELEASER_NAME = t.context.GITLAB_CI_RELEASER_NAME;
  process.env.GITLAB_CI_RELEASER_EMAIL = t.context.GITLAB_CI_RELEASER_EMAIL;
  commands.run.restore();
});

test('must commit a file and push the commit', (t) => {
  const commitAndPush = require('../../src/repository/commitAndPush');
  return commitAndPush('commitmessage', 'package.json', 'authenticationtoken')
    .then((createdCommitHash) => {
      t.is(commands.run.callCount, 5);
      t.true(commands.run.calledWithExactly('git -c user.email="master@example.com" -c user.name="developer" commit -m commitmessage -- package.json'));
      t.true(commands.run.calledWithExactly('git log -n 1 --pretty="%H"'));
      t.true(commands.run.calledWithExactly('git remote add gitlabcireleaser-15 https://developer:authenticationtoken@gitlab.com/developer/test-release-project.git'));
      t.true(commands.run.calledWithExactly('git push gitlabcireleaser-15 HEAD:master'));
      t.true(commands.run.calledWithExactly('git remote rm gitlabcireleaser-15'));
      t.is(createdCommitHash, 'commithash');
    });
});
